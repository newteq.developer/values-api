FROM centos AS base
RUN yum update -y
RUN yum install libicu -y
WORKDIR /app
RUN rm -rf /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS publish
WORKDIR /src
COPY . .
RUN rm -rf /app
RUN dotnet publish "ValuesApiDocker.sln" -c Release -o /app -r linux-x64

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["/app/ValuesApiDocker"]